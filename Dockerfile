FROM python:3.9.4-alpine3.13
WORKDIR /app

RUN apk update && \
    apk add --no-cache libpq && \
    apk add --no-cache --virtual build \
        postgresql-dev gcc python3-dev musl-dev && \
    pip --no-cache-dir install psycopg2==2.8.6 && \
    apk del --no-cache build

COPY ./requirements.txt ./requirements.txt
RUN pip install --no-cache-dir -r ./requirements.txt

COPY ./manage.py ./socnetwork ./
CMD ./manage.py runserver 0.0.0.0:8080
