This is an interview assigment for the position of Junior Python Developer at starnavi.io by Vladislav Poletaev.

## Endpoints list:

* Authentication: `POST /api/auth/`
* Renew token: `POST /api/auth/refresh/`
* List posts: `GET /api/posts/{?author_id,published_before,published_after}`
* Create post: `POST /api/posts/`
* Get post details: `GET /api/posts/<post_id>/`
* List users who liked the post: `GET /api/posts/<post_id>/liking/`
* Get current user details: `GET /api/user/`
* Create user: `POST /api/user/`
* Get user activity information: `GET /api/user/activity/`
* List posts, liked by the current user: `GET /api/user/likes/`
* Check weather user liked the post: `GET /api/user/likes/<post_id>/`
* Like the post: `PUT /api/user/likes/<post_id>/`
* Revoke the like of the post: `DELETE /api/user/likes/<post_id>/`
* Likes analytics: `GET /api/analytics/{?date_from,date_to}`

## Technology stack

* Django
* djangorestframework
* djangorestframework-simplejwt
* docker-compose
* postgresql

Running:

```
docker-compose up
```

Testing:

```
docker-compose exec django ./manage.py test socnetwork
```
