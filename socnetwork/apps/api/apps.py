from django.apps import AppConfig


class APIConfig(AppConfig):
    name = "socnetwork.apps.api"
    verbose_name = "REST API"
