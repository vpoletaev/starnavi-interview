from typing import Optional, Dict, Any
from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError as DjangoValidationError
from rest_framework.reverse import reverse
from rest_framework import serializers, validators
from socnetwork.apps.posts.models import Post

User = get_user_model()


class HateousLinks:
    """
    Collection fo HATEOUS links to be appended to the object returned
    by serializer.
    """

    def __init__(self, serializer: Optional[serializers.Serializer] = None):
        self.serializer = serializer
        self.links: Dict[str, str] = {}

    @property
    def data(self) -> Dict[str, Any]:
        """Returns serialized object"""

        if self.serializer is None:
            return self.links
        return {**self.serializer.data, **self.links}

    def add_link(self, name: str, url: str) -> None:
        """Adds a new link"""
        self.links[name] = url


class PasswordField(serializers.CharField):
    """
    A CharField that uses Django's password validation mechanism
    for validation.
    """

    def to_internal_value(self, data):
        password = super().to_internal_value(data)
        try:
            validate_password(password)
        except DjangoValidationError as error:
            raise serializers.ValidationError(str(error))
        return password


class UserSerializer(serializers.ModelSerializer):
    """Object representing a user throughout API"""

    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    password = PasswordField(write_only=True)
    email = serializers.EmailField(
        write_only=True,
        validators=[validators.UniqueValidator(queryset=User.objects.all())],
    )
    username = serializers.CharField(
        validators=[validators.UniqueValidator(queryset=User.objects.all())],
    )
    posts_list_url = serializers.SerializerMethodField()

    def get_posts_list_url(self, obj):
        url = reverse("posts_list", request=self.context["request"])
        return f"{url}?author_id={obj.id}"

    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "first_name",
            "last_name",
            "email",
            "password",
            "posts_list_url",
        ]


class PostSerializer(serializers.ModelSerializer):
    """Representation of the post used throughout API"""

    url = serializers.HyperlinkedIdentityField(
        view_name="post",
        lookup_url_kwarg="post_id",
    )
    liking_url = serializers.HyperlinkedIdentityField(
        view_name="post_liking",
        lookup_url_kwarg="post_id",
    )
    like_manage_url = serializers.HyperlinkedIdentityField(
        view_name="user_liked_post",
        lookup_url_kwarg="post_id",
    )
    author = UserSerializer(read_only=True)
    published_date = serializers.DateTimeField(read_only=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if not self.context["request"].user.is_authenticated:
            del representation["like_manage_url"]
        return representation

    class Meta:
        model = Post
        fields = [
            "id",
            "url",
            "author",
            "title",
            "contents",
            "published_date",
            "liking_url",
            "like_manage_url",
        ]


class UserActivitySerialzier(serializers.ModelSerializer):
    """Representation of user activity"""

    class Meta:
        model = User
        fields = ["auth_date", "access_date"]
