import datetime
from datetime import timedelta
from django.utils import timezone
from django.test import TestCase
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken
from socnetwork.apps.posts.models import Post, PostLike

User = get_user_model()


class AnalyticsTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        admin = User.objects.create_superuser("superuser")
        token = RefreshToken.for_user(admin)
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

    def test_requires_authentication(self):
        client = APIClient()
        response = client.get("/api/analytics/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_requires_staff_account(self):
        user = User.objects.create_user("not-admin")
        token = RefreshToken.for_user(user)

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")
        response = client.get("/api/analytics/")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_requires_date_from(self):
        response = self.client.get("/api/analytics/?date_to=2021-04-04")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_requires_date_to(self):
        response = self.client.get("/api/analytics/?date_from=2021-04-02")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_range_too_broad(self):
        # 101 days
        response = self.client.get(
            "/api/analytics/?date_from=2021-01-01&date_to=2021-04-11"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_range_invalid(self):
        response = self.client.get(
            "/api/analytics/?date_from=2021-01-08&date_to=2021-01-04"
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_range_okay(self):
        # 100 days
        response = self.client.get(
            "/api/analytics/?date_from=2021-01-01&date_to=2021-04-10"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class AnalyticsDataTestCase(AnalyticsTestCase):
    def setUp(self):
        super().setUp()

        users = [
            User.objects.create_user(f"user-{i}", f"u{i}@localhost", "pass")
            for i in range(2)
        ]
        posts = [
            Post.objects.create(
                author=users[0],
                title=f"Post {i}",
                contents="",
                published_date=timezone.now(),
            )
            for i in range(3)
        ]

        date = datetime.datetime(2021, 4, 1, 0, 0, 0, tzinfo=timezone.utc)
        PostLike.objects.create(
            post=posts[0],
            user=users[0],
            created_date=date + timedelta(hours=12),
            revoked_date=date + timedelta(days=2, hours=12),
        )
        PostLike.objects.create(
            post=posts[0],
            user=users[0],
            created_date=date + timedelta(days=4, hours=12),
        )
        PostLike.objects.create(
            post=posts[0],
            user=users[1],
            created_date=date + timedelta(days=1, hours=12),
            revoked_date=date + timedelta(days=3, hours=12),
        )
        PostLike.objects.create(
            post=posts[0],
            user=users[1],
            created_date=date + timedelta(days=3, hours=13),
            revoked_date=date + timedelta(days=3, hours=14),
        )
        PostLike.objects.create(
            post=posts[1],
            user=users[0],
            created_date=date + timedelta(days=1, hours=10),
            revoked_date=date + timedelta(days=1, hours=14),
        )
        PostLike.objects.create(
            post=posts[1],
            user=users[0],
            created_date=date + timedelta(days=2, hours=10),
            revoked_date=date + timedelta(days=2, hours=14),
        )
        PostLike.objects.create(
            post=posts[1],
            user=users[1],
            created_date=date + timedelta(days=3, hours=12),
        )
        PostLike.objects.create(
            post=posts[2],
            user=users[0],
            created_date=date + timedelta(days=1, hours=12),
            revoked_date=date + timedelta(days=3, hours=12),
        )
        PostLike.objects.create(
            post=posts[2],
            user=users[1],
            created_date=date + timedelta(days=1, hours=12),
            revoked_date=date + timedelta(days=2, hours=10),
        )
        PostLike.objects.create(
            post=posts[2],
            user=users[1],
            created_date=date + timedelta(days=2, hours=14),
            revoked_date=date + timedelta(days=3, hours=12),
        )

        # Test case:
        # Day 0:
        #  u0: liked post 0
        #  Total: 1 like, 0 unlikes
        # Day 1:
        #  u1: liked post 0
        #  u0: liked and unliked post 1; discarding
        #  u0: liked post 2
        #  u1: liked post 2
        #  Total: 3 likes, 0 unlikes
        # Day 2:
        #  u0: unliked post 0
        #  u0: liked and unliked post 1; discarding
        #  u1: unliked and liked post 2; discarding
        #  Total: 0 likes, 1 unlike
        # Day 3:
        #  u1: unliked post 0
        #  u1: liked and unliked post 0; discarding
        #  u1: liked post 1
        #  u0: unliked post 2
        #  u1: unliked post 2
        #  Total: 1 like, 3 unlikes
        # Day 4:
        #  u0: liked post 0
        #  Total: 1 like, 0 unlikes

    def test_three_days_range(self):
        response = self.client.get(
            "/api/analytics/?date_from=2021-04-02&date_to=2021-04-04"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(
            data,
            [
                {"date": "2021-04-02", "likes": 3, "unlikes": 0},
                {"date": "2021-04-03", "likes": 0, "unlikes": 1},
                {"date": "2021-04-04", "likes": 1, "unlikes": 3},
            ],
        )

    def test_zeros_right(self):
        response = self.client.get(
            "/api/analytics/?date_from=2021-04-04&date_to=2021-04-07"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(
            data,
            [
                {"date": "2021-04-04", "likes": 1, "unlikes": 3},
                {"date": "2021-04-05", "likes": 1, "unlikes": 0},
                {"date": "2021-04-06", "likes": 0, "unlikes": 0},
                {"date": "2021-04-07", "likes": 0, "unlikes": 0},
            ],
        )

    def test_zeros_left(self):
        response = self.client.get(
            "/api/analytics/?date_from=2021-03-30&date_to=2021-04-02"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(
            data,
            [
                {"date": "2021-03-30", "likes": 0, "unlikes": 0},
                {"date": "2021-03-31", "likes": 0, "unlikes": 0},
                {"date": "2021-04-01", "likes": 1, "unlikes": 0},
                {"date": "2021-04-02", "likes": 3, "unlikes": 0},
            ],
        )

    def test_zeros_all(self):
        response = self.client.get(
            "/api/analytics/?date_from=2021-08-05&date_to=2021-08-07"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(
            data,
            [
                {"date": "2021-08-05", "likes": 0, "unlikes": 0},
                {"date": "2021-08-06", "likes": 0, "unlikes": 0},
                {"date": "2021-08-07", "likes": 0, "unlikes": 0},
            ],
        )

    def test_single_day(self):
        response = self.client.get(
            "/api/analytics/?date_from=2021-04-03&date_to=2021-04-03"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.data
        self.assertEqual(data, [{"date": "2021-04-03", "likes": 0, "unlikes": 1}])
