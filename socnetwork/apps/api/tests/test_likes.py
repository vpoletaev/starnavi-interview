from abc import ABC
from datetime import datetime, timezone
from django.test import TestCase
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken
from socnetwork.apps.posts.models import Post, PostLike

User = get_user_model()


class BaseLikesTestCase(ABC, TestCase):
    def setUp(self):
        self.user = User.objects.create_user("user", "user@localhost", "password")
        user2 = User.objects.create_user("user2", "user2@localhost", "password")

        token = RefreshToken.for_user(self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        date = datetime(2021, 3, 2, 12, 0, 0, tzinfo=timezone.utc)

        self.posts = [
            Post.objects.create(
                author=user2,
                title=f"Post {i}",
                contents="Content",
                published_date=date,
            )
            for i in range(4)
        ]

        # Post 0: liked by another user
        PostLike.objects.create(post=self.posts[0], user=user2, created_date=date)

        # Post 1: liked by the user
        PostLike.objects.create(post=self.posts[1], user=self.user, created_date=date)

        # Post 2: liked but then unliked
        PostLike.objects.create(
            post=self.posts[2], user=self.user, created_date=date, revoked_date=date
        )

        # Post 3: liked, unliked, and then liked again
        PostLike.objects.create(
            post=self.posts[3], user=self.user, created_date=date, revoked_date=date
        )
        PostLike.objects.create(post=self.posts[3], user=self.user, created_date=date)

    def get_liked_posts_ids(self):
        response = self.client.get("/api/user/likes/")
        return set(post["id"] for post in response.data["results"])


class LikesListTestCase(BaseLikesTestCase):
    def test_requires_authentication(self):
        client = APIClient()
        response = client.get("/api/user/likes/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_lists_liked_posts(self):
        response = self.client.get("/api/user/likes/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        actual = set(post["title"] for post in response.data["results"])
        self.assertEqual(actual, {"Post 1", "Post 3"})


class CheckingLikedTestCase(BaseLikesTestCase):
    def test_requires_authentication(self):
        client = APIClient()
        response = client.get(f"/api/user/likes/{self.posts[0].id}/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_return_404_no_such_post(self):
        response = self.client.get(f"/api/user/likes/{self.posts[3].id + 100}/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_return_404_never_liked(self):
        response = self.client.get(f"/api/user/likes/{self.posts[0].id}/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_return_204_liked(self):
        response = self.client.get(f"/api/user/likes/{self.posts[1].id}/")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_return_404_unliked(self):
        response = self.client.get(f"/api/user/likes/{self.posts[2].id}/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_return_204_reliked(self):
        response = self.client.get(f"/api/user/likes/{self.posts[3].id}/")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class CreatingLikesTestCase(BaseLikesTestCase):
    def test_requires_authentication(self):
        client = APIClient()
        response = client.put(f"/api/user/likes/{self.posts[0].id}/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_return_404_no_such_post(self):
        response = self.client.put(f"/api/user/likes/{self.posts[3].id + 100}/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_succedes(self):
        for post in [self.posts[0], self.posts[2]]:
            with self.subTest(post.title):
                response = self.client.put(f"/api/user/likes/{post.id}/")
                self.assertEqual(response.status_code, status.HTTP_201_CREATED)
                self.assertIn(post.id, self.get_liked_posts_ids())

    def test_fails(self):
        for post in [self.posts[1], self.posts[3]]:
            with self.subTest(post.title):
                response = self.client.put(f"/api/user/likes/{post.id}/")
                self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class DeletingLikesTestCase(BaseLikesTestCase):
    def test_requires_authentication(self):
        client = APIClient()
        response = client.delete(f"/api/user/likes/{self.posts[0].id}/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_return_404_no_such_post(self):
        response = self.client.delete(f"/api/user/likes/{self.posts[3].id + 100}/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_succedes(self):
        for post in [self.posts[1], self.posts[3]]:
            with self.subTest(post.title):
                response = self.client.delete(f"/api/user/likes/{post.id}/")
                self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
                self.assertNotIn(post.id, self.get_liked_posts_ids())

    def test_fails(self):
        for post in [self.posts[0], self.posts[2]]:
            with self.subTest(post.title):
                response = self.client.delete(f"/api/user/likes/{post.id}/")
                self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
