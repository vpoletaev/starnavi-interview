from unittest import mock
from datetime import datetime, timezone
from django.test import TestCase
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken
from socnetwork.apps.posts.models import Post, PostLike

User = get_user_model()


class PostsListTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()

        self.users = []
        for i in range(3):
            user = User.objects.create_user(f"user-{i}", f"user{i}@localhost", "pass")
            user.first_name = f"{i}-f"
            user.last_name = f"{i}-l"
            user.save()
            self.users.append(user)

        posts_text = [
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            "Nullam ut massa arcu. Morbi sed nunc libero.",
            "Mauris sit amet dui scelerisque, convallis purus lacinia, dictum lectus.",
            "Donec aliquam lectus urna, pharetra viverra tortor vestibulum eget.",
            "Praesent semper, urna quis viverra consectetur, diam enim gravida tortor.",
            "Nunc sit amet mi condimentum, vehicula ante ullamcorper, tristique magna.",
            "Cras in enim porta, auctor quam vel, vestibulum leo.",
            "Aliquam varius tortor nec aliquam vulputate.",
            "Suspendisse ac orci eu nulla elementum tincidunt.",
            "Maecenas semper orci odio, eget iaculis massa sodales at.",
        ]
        post_date = [7, 2, 6, 10, 4, 1, 5, 3, 9, 8]

        self.posts = []
        for i, (post_content, date) in enumerate(zip(posts_text, post_date)):
            date = datetime(2021, 1, date, 12, 0, 0, tzinfo=timezone.utc)
            post = Post.objects.create(
                author=self.users[i % len(self.users)],
                title=post_content.split(" ")[0],
                contents=post_content,
                published_date=date,
            )
            self.posts.append(post)

    def test_returns_all_posts(self):
        response = self.client.get("/api/posts/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 10)
        results = response.data["results"]

        # Response format
        self.assertEqual(
            results[0],
            {
                "id": self.posts[3].id,
                "url": f"http://testserver/api/posts/{self.posts[3].id}/",
                "liking_url": f"http://testserver/api/posts/{self.posts[3].id}/liking/",
                "author": {
                    "id": self.users[0].id,
                    "username": "user-0",
                    "first_name": "0-f",
                    "last_name": "0-l",
                    "posts_list_url": f"http://testserver/api/posts/?author_id={self.users[0].id}",
                },
                "title": "Donec",
                "contents": "Donec aliquam lectus urna, pharetra viverra tortor vestibulum eget.",
                "published_date": "2021-01-10T12:00:00Z",
            },
        )

        # Response order
        titles = [doc["title"] for doc in results]
        self.assertEqual(
            titles,
            [
                "Donec",
                "Suspendisse",
                "Maecenas",
                "Lorem",
                "Mauris",
                "Cras",
                "Praesent",
                "Aliquam",
                "Nullam",
                "Nunc",
            ],
        )

    def test_filters_by_user_id(self):
        response = self.client.get(f"/api/posts/?author_id={self.users[1].id}")
        self.assertEqual(
            [doc["title"] for doc in response.data["results"]],
            ["Praesent", "Aliquam", "Nullam"],
        )

    def test_filters_by_date_range_after(self):
        response = self.client.get("/api/posts/?published_after=2021-01-06T00:00:00Z")
        self.assertEqual(
            [doc["title"] for doc in response.data["results"]],
            ["Donec", "Suspendisse", "Maecenas", "Lorem", "Mauris"],
        )

    def test_filters_by_date_range_before(self):
        response = self.client.get("/api/posts/?published_before=2021-01-06T00:00:00Z")
        self.assertEqual(
            [doc["title"] for doc in response.data["results"]],
            ["Cras", "Praesent", "Aliquam", "Nullam", "Nunc"],
        )

    def test_validates_parameter_types(self):
        invalid_params = [
            "author_id=abc",
            "published_before=abc",
            "published_after=abc",
        ]
        for param in invalid_params:
            with self.subTest(param):
                response = self.client.get(f"/api/posts/?{param}")
                self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class PostCreateTestCase(TestCase):
    POST_DATA = {
        "title": "Lorem",
        "contents": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    }

    def setUp(self):
        self.user = User.objects.create_user("user", "user@localhost", "password")
        self.user.first_name = "F"
        self.user.last_name = "L"
        self.user.save()

        token = RefreshToken.for_user(self.user)
        self.client = APIClient()
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

    def test_requires_authentication(self):
        client = APIClient()
        response = client.post("/api/posts/", data=PostCreateTestCase.POST_DATA)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_creates_post(self):
        now = datetime(2021, 3, 2, 12, 0, 0, tzinfo=timezone.utc)
        with mock.patch("socnetwork.apps.api.views.posts.timezone") as mock_timezone:
            mock_timezone.now.return_value = now
            response = self.client.post(
                "/api/posts/", data=PostCreateTestCase.POST_DATA
            )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.data["author"],
            {
                "id": self.user.id,
                "username": "user",
                "first_name": "F",
                "last_name": "L",
                "posts_list_url": f"http://testserver/api/posts/?author_id={self.user.id}",
            },
        )
        self.assertEqual(response.data["title"], PostCreateTestCase.POST_DATA["title"])
        self.assertEqual(
            response.data["contents"], PostCreateTestCase.POST_DATA["contents"]
        )
        self.assertEqual(response.data["published_date"], "2021-03-02T12:00:00Z")

    def test_validates_required_fiedlds(self):
        for field in PostCreateTestCase.POST_DATA:
            data = {**PostCreateTestCase.POST_DATA}
            del data[field]
            with self.subTest(f"Missing {field}"):
                response = self.client.post("/api/posts/", data=data)
                self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_validates_max_title_length(self):
        data = {**PostCreateTestCase.POST_DATA, "title": "A" * 129}
        response = self.client.post("/api/posts/", data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class PostDetailsTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user("user", "user@localhost", "pass")
        self.user.first_name = "F"
        self.user.last_name = "L"
        self.user.save()

        self.post = Post.objects.create(
            author=self.user,
            title="Lorem",
            contents="Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            published_date=datetime(2021, 3, 2, 12, 0, 0, tzinfo=timezone.utc),
        )

    def test_returns_post(self):
        client = APIClient()
        response = client.get(f"/api/posts/{self.post.id}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            {
                "id": self.post.id,
                "url": f"http://testserver/api/posts/{self.post.id}/",
                "liking_url": f"http://testserver/api/posts/{self.post.id}/liking/",
                "author": {
                    "id": self.user.id,
                    "username": "user",
                    "first_name": "F",
                    "last_name": "L",
                    "posts_list_url": f"http://testserver/api/posts/?author_id={self.user.id}",
                },
                "title": "Lorem",
                "contents": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                "published_date": "2021-03-02T12:00:00Z",
            },
        )

    def test_returns_post_authorized(self):
        token = RefreshToken.for_user(self.user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        response = client.get(f"/api/posts/{self.post.id}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            {
                "id": self.post.id,
                "url": f"http://testserver/api/posts/{self.post.id}/",
                "liking_url": f"http://testserver/api/posts/{self.post.id}/liking/",
                "like_manage_url": f"http://testserver/api/user/likes/{self.post.id}/",
                "author": {
                    "id": self.user.id,
                    "username": "user",
                    "first_name": "F",
                    "last_name": "L",
                    "posts_list_url": f"http://testserver/api/posts/?author_id={self.user.id}",
                },
                "title": "Lorem",
                "contents": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                "published_date": "2021-03-02T12:00:00Z",
            },
        )

    def test_returns_404(self):
        client = APIClient()
        response = client.get(f"/api/posts/{self.post.id + 100}/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class PostLikingListTestCase(TestCase):
    def test_returns_404_when_post_does_not_exist(self):
        client = APIClient()
        response = client.get("/api/posts/123/linking/")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_returns_users(self):
        users = []
        for i in range(4):
            user = User.objects.create_user(f"user-{i}", f"user{i}@localhost", "pass")
            user.first_name = f"{i}-f"
            user.last_name = f"{i}-l"
            user.save()
            users.append(user)

        post = Post.objects.create(
            author=users[0],
            title="A",
            contents="B",
            published_date=datetime(2021, 3, 2, 12, 0, 0, tzinfo=timezone.utc),
        )

        # User 0: never liked the post

        # User 1: liked the post
        PostLike.objects.create(
            post=post,
            user=users[1],
            created_date=datetime(2021, 3, 2, 12, 0, 0, tzinfo=timezone.utc),
        )

        # User 2: liked the post and then revoked the like
        PostLike.objects.create(
            post=post,
            user=users[2],
            created_date=datetime(2021, 3, 2, 12, 0, 0, tzinfo=timezone.utc),
            revoked_date=datetime(2021, 3, 2, 13, 0, 0, tzinfo=timezone.utc),
        )

        # User 3: same as user 2 but liked the post again
        PostLike.objects.create(
            post=post,
            user=users[3],
            created_date=datetime(2021, 3, 2, 12, 0, 0, tzinfo=timezone.utc),
            revoked_date=datetime(2021, 3, 2, 13, 0, 0, tzinfo=timezone.utc),
        )
        PostLike.objects.create(
            post=post,
            user=users[3],
            created_date=datetime(2021, 3, 2, 14, 0, 0, tzinfo=timezone.utc),
        )

        client = APIClient()
        response = client.get(f"/api/posts/{post.id}/liking/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        liking_users = [user["id"] for user in response.data["results"]]
        self.assertEqual(liking_users, [users[1].id, users[3].id])
