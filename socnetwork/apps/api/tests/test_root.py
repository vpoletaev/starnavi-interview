from django.test import TestCase
from django.contrib.auth import get_user_model
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken

User = get_user_model()


class RootLinksTestCase(TestCase):
    def test_unauthorized(self):
        client = APIClient()
        response = client.get("/api/")
        self.assertEqual(set(response.data.keys()), {"auth_url", "posts_list_url"})

    def test_authorized(self):
        user = User.objects.create_user("user")
        token = RefreshToken.for_user(user)

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")
        response = client.get("/api/")
        self.assertEqual(set(response.data.keys()), {"user_url", "posts_list_url"})

    def test_admin(self):
        user = User.objects.create_superuser("user")
        token = RefreshToken.for_user(user)

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")
        response = client.get("/api/")
        self.assertEqual(
            set(response.data.keys()), {"user_url", "posts_list_url", "analytics_url"}
        )
