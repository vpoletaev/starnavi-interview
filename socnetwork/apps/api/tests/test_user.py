from unittest import mock
from datetime import datetime, timezone
from django.test import TestCase
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken

User = get_user_model()


class UserViewTestCase(TestCase):
    def test_requires_authentication(self):
        client = APIClient()
        response = client.get("/api/user/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_returns_user(self):
        user = User.objects.create_user("user", first_name="John", last_name="Doe")
        token = RefreshToken.for_user(user)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")
        response = client.get("/api/user/")

        self.assertEqual(
            response.data,
            {
                "id": user.id,
                "username": "user",
                "last_name": "Doe",
                "first_name": "John",
                "likes_url": "http://testserver/api/user/likes/",
                "activity_url": "http://testserver/api/user/activity/",
                "posts_list_url": f"http://testserver/api/posts/?author_id={user.id}",
            },
        )


class UserCreationTestCase(TestCase):
    VALID_DATA = {
        "username": "user4",
        "email": "user4@localhost",
        "password": "FAGjjxPDZX6H9aBLAhrr5Aqc",
        "first_name": "John",
        "last_name": "Doe",
    }

    def setUp(self):
        self.users = [
            User.objects.create_user("user1", "user1@localhost", "pass"),
            User.objects.create_user("user2", "user2@localhost", "pass"),
            User.objects.create_user("user3", "user3@localhost", "pass"),
        ]

    def test_requires_not_authenticated(self):
        token = RefreshToken.for_user(self.users[0])
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")
        response = client.post("/api/user/", data={})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_success(self):
        client = APIClient()
        response = client.post("/api/user/", data=UserCreationTestCase.VALID_DATA)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        user = User.objects.get(username="user4")
        self.assertEqual(user.email, "user4@localhost")
        self.assertEqual(user.first_name, "John")
        self.assertEqual(user.last_name, "Doe")

    def test_weak_password(self):
        client = APIClient()
        response = client.post(
            "/api/user/",
            data={**UserCreationTestCase.VALID_DATA, "password": "qwerty123"},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_duplicate_email(self):
        client = APIClient()
        response = client.post(
            "/api/user/",
            data={**UserCreationTestCase.VALID_DATA, "email": "user2@localhost"},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_duplicate_username(self):
        client = APIClient()
        response = client.post(
            "/api/user/",
            data={**UserCreationTestCase.VALID_DATA, "username": "user2"},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_email(self):
        client = APIClient()
        response = client.post(
            "/api/user/",
            data={**UserCreationTestCase.VALID_DATA, "email": "user2"},
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_missing_field(self):
        client = APIClient()
        for field in UserCreationTestCase.VALID_DATA:
            data = {**UserCreationTestCase.VALID_DATA}
            del data[field]
            response = client.post("/api/user/", data=data)
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UserActivityTestCase(TestCase):
    def test_requires_authentication(self):
        client = APIClient()
        response = client.get("/api/user/activity/")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_success(self):
        User.objects.create_user("user", "user@localhost", "pass")
        client = APIClient()

        # Logging in
        with mock.patch("socnetwork.apps.users.views.timezone") as mock_timezone:
            mock_timezone.now.return_value = datetime(
                2021, 4, 12, 12, 0, 0, tzinfo=timezone.utc
            )
            response = client.post(
                "/api/auth/", data={"username": "user", "password": "pass"}
            )
        client.credentials(HTTP_AUTHORIZATION=f"Bearer {response.data['access']}")

        # Making request
        with mock.patch("socnetwork.apps.users.middleware.timezone") as mock_timezone:
            mock_timezone.now.return_value = datetime(
                2021, 4, 12, 12, 1, 0, tzinfo=timezone.utc
            )
            client.get("/api/posts/")

        # Fetching activity
        response = client.get("/api/user/activity/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["auth_date"], "2021-04-12T12:00:00Z")
        self.assertEqual(response.data["access_date"], "2021-04-12T12:01:00Z")
