from django.urls import path
from socnetwork.apps.users.urls import urlpatterns as users_urlpatterns
from socnetwork.apps.api import views

urlpatterns = [
    path("", views.RootView.as_view(), name="root"),
    path("posts/", views.PostsListView.as_view(), name="posts_list"),
    path("posts/<int:post_id>/", views.PostView.as_view(), name="post"),
    path(
        "posts/<int:post_id>/liking/",
        views.PostLikingView.as_view(),
        name="post_liking",
    ),
    path("user/", views.UserView.as_view(), name="user"),
    path("user/likes/", views.LikesListView.as_view(), name="user_likes"),
    path("user/activity/", views.UserActivityView.as_view(), name="user_activity"),
    path(
        "user/likes/<int:post_id>/",
        views.LikedPostView.as_view(),
        name="user_liked_post",
    ),
    path("analytics/", views.AnalyticsView.as_view(), name="analytics"),
] + users_urlpatterns
