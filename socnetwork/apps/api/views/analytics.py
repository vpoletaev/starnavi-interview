from typing import Iterable
from itertools import starmap
from datetime import date, timedelta
from dataclasses import dataclass
from django.db import connection
from rest_framework import serializers
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser


# pylint: disable=abstract-method
class AnalyticsParamsSerializer(serializers.Serializer):
    date_from = serializers.DateField(required=True)
    date_to = serializers.DateField(required=True)

    def validate(self, attrs):
        attrs = super().validate(attrs)
        differece = (attrs["date_to"] - attrs["date_from"]).days
        if differece < 0:
            raise serializers.ValidationError("date_to cannot be before date_from")
        if differece >= 100:
            raise serializers.ValidationError("date range is too broad")
        return attrs


@dataclass
class AnalyticsResult:
    """
    Representation of the day's statistics returned by `/api/analytics/`

    Attributes:
    date -- day in range `day_from`..`day_to` for which analytics values are computed
    likes -- the number of likes created that day
    unlikes -- the number of likes revoked that day
    """

    date: date
    likes: int
    unlikes: int

    @staticmethod
    def fill_gaps(
        date_from: date, date_to: date, values: Iterable["AnalyticsResult"]
    ) -> Iterable["AnalyticsResult"]:
        """
        Inserts empty result objects into the iterable `values`. This
        function ensures that for every day in range `date_from`..`date_to`
        there exists an AnalyticsResult.

        Values of iterable `values` are expected to be ordered by `date`.
        """

        day = date_from
        next_day = timedelta(days=1)
        for entry in values:
            while day < entry.date:
                yield AnalyticsResult(day, 0, 0)
                day += next_day
            yield entry
            day = entry.date + next_day
        while day <= date_to:
            yield AnalyticsResult(day, 0, 0)
            day += next_day


class AnalyticsResultSerializer(serializers.Serializer):
    date = serializers.DateField()
    likes = serializers.IntegerField()
    unlikes = serializers.IntegerField()


class AnalyticsView(APIView):
    """
    Returns the analytics information.

    This endpoint returnes an object for each day in the range (inclusive)
    specified by `date_from` and `date_to` no longer than 100 days. These
    objects contain:

     * `date` (date): day in ISO 8601 format
     * `likes` (int): number of likes that were created that day
     * `unlikes` (int): number of likes revoked that day

    If post was liked and unliked by the user within single day, this action
    is ignored and not inclided in the statistics
    """

    permission_classes = [IsAdminUser]

    def get(self, request):
        params_serializer = AnalyticsParamsSerializer(data=request.query_params)
        params_serializer.is_valid(raise_exception=True)
        params = params_serializer.validated_data

        with connection.cursor() as cursor:
            cursor.execute(
                """
                SELECT
                    day,
                    COUNT(likes) FILTER (WHERE likes = 1) AS likes,
                    COUNT(likes) FILTER (WHERE likes = -1) AS unlikes
                FROM (
                    SELECT day, SUM(likes_delta) AS likes, user_id, post_id
                    FROM (
                        SELECT DATE(created_date) AS day, 1 AS likes_delta, user_id, post_id
                            FROM posts_postlike
                        UNION ALL
                        SELECT DATE(revoked_date) AS day, -1 AS likes_delta, user_id, post_id
                            FROM posts_postlike
                            WHERE revoked_date IS NOT null
                    ) AS deltas_query
                    WHERE day BETWEEN %s AND %s
                    GROUP BY day, user_id, post_id
                ) AS delta_sum_query
                GROUP BY day
                ORDER BY day
                """,
                [params["date_from"], params["date_to"]],
            )
            results = list(
                AnalyticsResult.fill_gaps(
                    params["date_from"],
                    params["date_to"],
                    starmap(AnalyticsResult, cursor.fetchall()),
                )
            )

        serializer = AnalyticsResultSerializer(results, many=True)
        return Response(serializer.data)
