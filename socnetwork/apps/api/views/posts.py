from django.utils import timezone
from django.http import Http404
from django.contrib.auth import get_user_model
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import ListCreateAPIView, ListAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework import serializers
from socnetwork.apps.posts.models import Post
from socnetwork.apps.api.serializers import PostSerializer, UserSerializer

User = get_user_model()


# pylint: disable=abstract-method
class PostsListParamsSerializer(serializers.Serializer):
    """
    Serializer for `GET /api/posts/` query parameters
    """

    author_id = serializers.IntegerField(required=False)
    published_before = serializers.DateTimeField(required=False)
    published_after = serializers.DateTimeField(required=False)


class PostsListView(ListCreateAPIView):
    """
    Endpoint lists (on *GET*) all posts in reverse chronological order
    or creates (on *POST*) a new post signed by the authenticad user.

    When listing the posts the following query parameters are recognized:

     * `author_id` (int) List posts only created by the user with that id
     * `published_after` (datetime) List posts created after (strictly)
       specified date
     * `published_before` (datetime) List posts created before (strictly)
       specified date

    When creating the post, this enpoint expects an object with fields
    `title` and `contents` (strings) containing the post's title and
    contents respectfully.
    """

    serializer_class = PostSerializer
    queryset = Post.objects.select_related("author").order_by("-published_date")
    permission_classes = [IsAuthenticatedOrReadOnly]

    def filter_queryset(self, queryset):
        filters = PostsListParamsSerializer(data=self.request.query_params)
        filters.is_valid(raise_exception=True)
        filters_data = filters.validated_data

        if "author_id" in filters_data:
            queryset = queryset.filter(author__pk=filters_data["author_id"])
        if "published_after" in filters_data:
            queryset = queryset.filter(
                published_date__gt=filters_data["published_after"]
            )
        if "published_before" in filters_data:
            queryset = queryset.filter(
                published_date__lt=filters_data["published_before"]
            )

        return queryset

    def perform_create(self, serializer):
        return serializer.save(
            author_id=self.request.user.id,
            published_date=timezone.now(),
        )


class PostView(APIView):
    """
    Returns a post with a specified numeric id.
    """

    def get(self, request, post_id):
        try:
            post = Post.objects.get(id=post_id)
        except Post.DoesNotExist as post_ex:
            raise Http404() from post_ex
        serializer = PostSerializer(post, context={"request": request})
        return Response(serializer.data)


class PostLikingView(ListAPIView):
    """
    Returns all users who has liked the post with specified id. On how to
    create and revoke likes see `/api/user/likes/` documentation.

    Returns 404 if referenced post does not exist.
    """

    serializer_class = UserSerializer

    def get_queryset(self):
        try:
            post = Post.objects.get(id=self.kwargs["post_id"])
        except Post.DoesNotExist as post_ex:
            raise Http404() from post_ex

        return User.objects.filter(
            is_active=True, postlike__post=post, postlike__revoked_date__isnull=True
        )
