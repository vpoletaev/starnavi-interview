from rest_framework.views import APIView
from rest_framework.reverse import reverse
from rest_framework.response import Response
from socnetwork.apps.api.serializers import HateousLinks


class RootView(APIView):
    """Root view of the REST API. Lists links to high-level resources"""

    def get(self, request):
        links = HateousLinks()
        if request.user.is_staff:
            links.add_link("analytics_url", reverse("analytics", request=request))
        if request.user.is_authenticated:
            links.add_link("user_url", reverse("user", request=request))
        else:
            links.add_link("auth_url", reverse("token_obtain_pair", request=request))
        links.add_link("posts_list_url", reverse("posts_list", request=request))
        return Response(links.data)
