from django.db import transaction
from django.utils import timezone
from django.http import Http404
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated, BasePermission
from socnetwork.apps.posts.models import Post, PostLike
from socnetwork.apps.api.serializers import (
    PostSerializer,
    UserSerializer,
    UserActivitySerialzier,
    HateousLinks,
)

User = get_user_model()


class NotAuthenticatedPut(BasePermission):
    """
    Permission class that prevents authenticated users from gaining access to
    *POST* method.
    """

    def has_permission(self, request, _view):
        authenticated = request.user.is_authenticated
        return not authenticated if request.method == "POST" else authenticated


class UserView(APIView):
    """
    This endpoint represents the currently authenticated user.

    On *GET* endpoint returns an object representing the user.

    On *POST* the new user is created. `email` and `password` fields
    are required in that case.
    """

    serializer_class = UserSerializer
    permission_classes = [NotAuthenticatedPut]

    def get(self, request):
        user = User.objects.get(id=request.user.id)

        serializer = HateousLinks(UserSerializer(user, context={"request": request}))
        serializer.add_link("likes_url", reverse("user_likes", request=request))
        serializer.add_link("activity_url", reverse("user_activity", request=request))
        return Response(serializer.data)

    def post(self, request):
        user = UserSerializer(data=request.data)
        user.is_valid(raise_exception=True)
        data = user.validated_data

        User.objects.create_user(
            data["username"],
            data["email"],
            data["password"],
            first_name=data["first_name"],
            last_name=data["last_name"],
        )
        return Response(status=status.HTTP_201_CREATED)


class UserActivityView(APIView):
    """
    Returns the activity information for the current user, namely
    the date and time of the last successful login and the date and
    time of the latest authenticated request.
    """

    permission_classes = [IsAuthenticated]

    def get(self, request):
        user = User.objects.get(id=self.request.user.id)
        serializer = UserActivitySerialzier(user)
        return Response(serializer.data)


class LikesListView(ListAPIView):
    """
    Returns the list of posts, liked by the current user
    """

    serializer_class = PostSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Post.objects.filter(
            postlike__user_id=self.request.user.id,
            postlike__revoked_date__isnull=True,
        ).select_related("author")


class AlreadyLiked(APIException):
    """
    An exception that is returned when user tries to like the post
    that he/she has already liked.
    """

    status_code = 403
    default_detail = "The post is already liked by the user"
    default_code = "already_liked"


class LikedPostView(APIView):
    """
    This endpoint is used to manage user's liked posts.

    When performing `GET /api/user/likes/<post_id>/`, the status code
    indicates weather the user has liked the post with specific id
    (204 No content if true, 404 Not Found otherwise).

    When performing *POST* to the same URL, the post is added to the
    list of posts liked by the user. If the post has already been liked,
    the endpoint responds with 403 `already_liked` error.

    On *DELETE* the post is removed from the list of liked posts.
    """

    permission_classes = [IsAuthenticated]

    def get(self, request, post_id):
        try:
            PostLike.objects.get(
                user_id=request.user.id, post_id=post_id, revoked_date__isnull=True
            )
        except PostLike.DoesNotExist as post_like_ex:
            raise Http404 from post_like_ex
        else:
            return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, post_id):
        try:
            post = Post.objects.get(id=post_id)
        except Post.DoesNotExist as post_ex:
            raise Http404 from post_ex

        try:
            PostLike.objects.get(
                user_id=request.user.id, post=post, revoked_date__isnull=True
            )
        except PostLike.DoesNotExist:
            pass
        else:
            raise AlreadyLiked

        PostLike.objects.create(
            user_id=request.user.id, post=post, created_date=timezone.now()
        )
        return Response(status=status.HTTP_201_CREATED)

    def delete(self, request, post_id):
        with transaction.atomic():
            try:
                like = PostLike.objects.select_for_update().get(
                    user_id=request.user.id, post_id=post_id, revoked_date__isnull=True
                )
            except PostLike.DoesNotExist as post_like_ex:
                raise Http404 from post_like_ex

            like.revoked_date = timezone.now()
            like.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
