from django.apps import AppConfig


class PostsConfig(AppConfig):
    name = "socnetwork.apps.posts"
    verbose_name = "Posts data"
