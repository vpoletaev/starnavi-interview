from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Post(models.Model):
    """Model representing a post, created by the user."""

    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=128)
    contents = models.TextField()
    published_date = models.DateTimeField()


class PostLike(models.Model):
    """
    PostLike describes the relationship between the user and posts
    that he/she liked or unliked.

    When the user likes the post a new PostLike is created with
    `revoked_date` set to NULL. If the user unliked the post, this
    field is set with the current date and time. Users of this models
    MUST ignore all rows with revoked_date not set to NULL when
    determining weather user likes the post or not.

    PostLikes are never deleted (except when user or post is deleted)
    in order to preserve the history of the application state for the
    purposes of analytics.
    """

    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField()
    revoked_date = models.DateTimeField(null=True, default=None)
