from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = "socnetwork.apps.users"
