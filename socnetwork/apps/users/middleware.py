from django.utils import timezone
from django.contrib.auth import get_user_model
from rest_framework_simplejwt.authentication import JWTAuthentication

User = get_user_model()


class LogUserRequestsMiddleware:
    """
    Middleware that updated User's access_date field on each
    authenticated request.

    Note that using this middleware valididates HTTP semantics:
    GET, HEAD, etc. methods cannot be considered idempotent as they
    change the User's state. Some other architectural pattern should
    be considered if the requirement constraints change.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        user = request.user
        authenticated = request.user.is_authenticated
        if not authenticated:
            auth = JWTAuthentication()
            auth_data = auth.authenticate(request)
            if auth_data is not None:
                user = auth_data[0]
                authenticated = True

        if authenticated:
            User.objects.filter(id=user.id).update(access_date=timezone.now())

        return response
