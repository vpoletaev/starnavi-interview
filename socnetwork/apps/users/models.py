from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """
    A custom user model that contains the latest dates and
    times when user performed certain acitivities.
    """

    access_date = models.DateTimeField(
        null=True,
        help_text=(
            "The date and time of the user's latest access to the service. "
            "Can be null if the user has never made any request after the "
            "initial login or if there were no login at all"
        ),
    )
    auth_date = models.DateTimeField(
        null=True,
        help_text=(
            "The date and time of the latest successful login by the user. "
            "Can be null if there were no such login."
        ),
    )
