from unittest import mock
from datetime import datetime, timezone, timedelta
from django.test import TestCase
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.authentication import JWTAuthentication

User = get_user_model()


class LoggingRequestsTestCase(TestCase):
    def setUp(self):
        self.user1 = User.objects.create_user("user1", "user1@localhost", "pass")
        self.user2 = User.objects.create_user("user2", "user2@localhost", "pass")

    def test_changes_access_date(self):
        now = datetime(2021, 4, 12, 12, 00, 00, tzinfo=timezone.utc)

        token = RefreshToken.for_user(self.user1)
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION=f"Bearer {token.access_token}")

        with mock.patch("socnetwork.apps.users.middleware.timezone") as mock_timezone:
            mock_timezone.now.return_value = now
            client.get("/")

        self.user1.refresh_from_db()
        self.assertEqual(self.user1.access_date, now)

        self.user2.refresh_from_db()
        self.assertIsNone(self.user2.access_date)


class AuthenticationTestCase(TestCase):
    def test_aquires_token(self):
        user = User.objects.create_user("user", "user@localhost", "password")
        client = APIClient()

        now = datetime(2021, 4, 12, 12, 00, 00, tzinfo=timezone.utc)
        with mock.patch("socnetwork.apps.users.views.timezone") as mock_timezone:
            mock_timezone.now.return_value = now
            response = client.post(
                "/api/auth/",
                data={"username": "user", "password": "password"},
                format="json",
            )

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        authentication = JWTAuthentication()
        auth_user = authentication.get_user(
            authentication.get_validated_token(response.data["access"])
        )
        self.assertEqual(auth_user, user)

        user.refresh_from_db()
        self.assertEqual(user.auth_date, now)

    def test_fails_to_authenticate_non_existant_user(self):
        client = APIClient()
        response = client.post(
            "/api/auth/",
            data={"username": "unknown", "password": "password"},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_refreshes_token(self):
        user = User.objects.create_user("user", "user@localhost", "password")
        refresh = RefreshToken.for_user(user)

        client = APIClient()
        response = client.post(
            "/api/auth/refresh/",
            data={"refresh": str(refresh)},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        authentication = JWTAuthentication()
        auth_user = authentication.get_user(
            authentication.get_validated_token(response.data["access"])
        )
        self.assertEqual(auth_user, user)

    def test_refresh_failes_if_token_expires(self):
        user = User.objects.create_user("user", "user@localhost", "password")
        refresh = RefreshToken.for_user(user)
        refresh.set_exp(from_time=datetime.now() - timedelta(days=5))

        client = APIClient()
        response = client.post(
            "/api/auth/refresh/",
            data={"refresh": str(refresh)},
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
