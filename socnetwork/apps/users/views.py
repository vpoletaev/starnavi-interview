from django.utils import timezone
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.views import (
    TokenObtainPairView as JWTTokenObtainPairView,
)

User = get_user_model()


class TokenObtainPairView(JWTTokenObtainPairView):
    def post(self, request, *args, **kwargs):
        """Updates User's auth_date after successfull login."""
        response = super().post(request, *args, **kwargs)

        if response.status_code == status.HTTP_200_OK:
            auth = JWTAuthentication()
            token = auth.get_validated_token(response.data["access"])
            user = auth.get_user(token)
            User.objects.filter(id=user.id).update(auth_date=timezone.now())

        return response
